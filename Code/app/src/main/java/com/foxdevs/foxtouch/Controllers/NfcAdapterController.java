package com.foxdevs.foxtouch.Controllers;

import android.content.Context;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.util.Log;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import static android.widget.Toast.LENGTH_LONG;

public class NfcAdapterController
{
    private static final String MIME_TEXT_PLAIN = "text/plain";

    public static void IsValidNfc(Context context)    {
        NfcAdapter mNfcAdapter = NfcAdapter.getDefaultAdapter(context);
        String strContenido="";

        if (mNfcAdapter == null) {strContenido ="El dispositivo no soporta NFC";
        }else { strContenido = mNfcAdapter.isEnabled() ? "NFC esta activo" : "NFC esta desactivado.";}

        Toast.makeText(context,strContenido, LENGTH_LONG).show();
    }

    public static String GetNfcAction(Intent intent) {
        String action = intent.getAction();
        String NfcAction="";
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            String type = intent.getType();
            if (MIME_TEXT_PLAIN.equals(type)) {
                Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                return ReadTag(tag);
            } else {
                Log.d("Log Nfc", "Wrong mime type: " + type);
            }
        } else if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {
            // In case we would still use the Tech Discovered Intent
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String[] techList = tag.getTechList();
            String searchedTech = Ndef.class.getName();

            for (String tech : techList) {
                if (searchedTech.equals(tech)) {
                    return ReadTag(tag);
                }
            }
        }
        return NfcAction;
    }
    private static String ReadTag(Tag tag)    {
        Ndef ndef = Ndef.get(tag);
        if (ndef == null) {
            return null;
        }

        NdefMessage ndefMessage = ndef.getCachedNdefMessage();

        NdefRecord[] records = ndefMessage.getRecords();
        for (NdefRecord ndefRecord : records) {
            if (ndefRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN && Arrays.equals(ndefRecord.getType(), NdefRecord.RTD_TEXT)) {
                try {
                    return readText(ndefRecord);
                } catch (UnsupportedEncodingException e) {
                    Log.e("Log Nfc", "Unsupported Encoding", e);
                }
            }
        }
        return null;
    }
    private static String readText(NdefRecord record) throws UnsupportedEncodingException {
        byte[] payload = record.getPayload();
        String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";
        int languageCodeLength = payload[0] & 0063;
        return new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
    }
}