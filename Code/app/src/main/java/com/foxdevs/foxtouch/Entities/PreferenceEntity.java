package com.foxdevs.foxtouch.Entities;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.util.Date;

public class PreferenceEntity {
    public String Nif;
    public String Usuario;
    public Date FechaUltimoFichaje;

    public String NifCifrado()
    {
        byte[] data = new byte[0];
        try {
            data = Nif.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);
        return base64.substring(0,base64.length()-1);
    }
}
