package com.foxdevs.foxtouch;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;

import com.foxdevs.foxtouch.Controllers.NfcAdapterController;
import com.foxdevs.foxtouch.Controllers.NotificationController;
import com.foxdevs.foxtouch.Controllers.FichajesController;
import com.foxdevs.foxtouch.Controllers.PreferencesController;
import com.foxdevs.foxtouch.Entities.PreferenceEntity;

public class MainActivity extends AppCompatActivity {
    public static final String URL = "http://sgpresencia.serinfer.com:6362/api";
    public static final String NFC_INVOKE_ACTION = "Nfc_FoxTouch_Action_Checking";
    private  PreferenceEntity preferences = new PreferenceEntity();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        preferences = PreferencesController.LoadPreferents(this.getApplicationContext());
        ExecNfcParameters();
    }

    private void ExecNfcParameters() {
        if (NFC_INVOKE_ACTION.equals(NfcAdapterController.GetNfcAction(getIntent()))) {
            long segundosAnhadidos = 15 * 1000;
            if ((preferences.FechaUltimoFichaje.getTime()+segundosAnhadidos)< System.currentTimeMillis() )
            {
                Fichar();
                PreferencesController.SavePreferents(this.getApplicationContext(),preferences,false);
            }
            finishAffinity();
        } else {
           NfcAdapterController.IsValidNfc(this.getApplicationContext());
           MostarInformacionPantalla();
        }
    }

    public void Save(View view) {
        preferences.Usuario= ((EditText) findViewById(R.id.etUsuario)).getText().toString().trim();
        preferences.Nif=((EditText) findViewById(R.id.etNif)).getText().toString().trim();
        PreferencesController.SavePreferents(this.getApplicationContext(),preferences,true  );
        //Comprobar Estado
        EstadoFichaje();
    }

    private void MostarInformacionPantalla() {
        //Cargar Config
        ((EditText) findViewById(R.id.etUsuario)).setText(preferences.Usuario);
        ((EditText) findViewById(R.id.etNif)).setText(preferences.Nif);
        //Comprobar Estado
        EstadoFichaje();
    }

    private void Fichar() {
        try {
            FichajesController fichajesController = new FichajesController();
            fichajesController.SetFichar(URL, preferences.Usuario, preferences.NifCifrado());
            ActualizarEstadoSwitch(fichajesController.Conectado);
            NotificationController.show(fichajesController.Conectado, this);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void EstadoFichaje() {
        try {
            if (preferences.Usuario != null && !preferences.Usuario.isEmpty() ) {
                FichajesController fichajesController = new FichajesController();
                fichajesController.GetEstadoFichaje(URL, preferences.Usuario, preferences.NifCifrado());
                ActualizarEstadoSwitch(fichajesController.Conectado);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void ActualizarEstadoSwitch(boolean fichado) {
        Switch onOffSwitch = findViewById(R.id.swFichado);
        onOffSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {Log.v("Switch State=", "" + isChecked);    }

        });
        onOffSwitch.setChecked(fichado);
    }
}
