package com.foxdevs.foxtouch.Controllers;

public class FichajesController {
    public Boolean Conectado=false;
    
    public void GetEstadoFichaje(String urlAdress, String usuario, String nif) throws InterruptedException {
        class PostRunableClass implements Runnable {
            private String UrlAdress;
            private String Usuario;
            private String Nif;
            private PostRunableClass(String urlAdress, String usuario, String nif) { UrlAdress = urlAdress; Usuario = usuario; Nif = nif;}
            public void run() {
                ApiServiveController apiServiveController = new ApiServiveController();
                String url = UrlAdress + "/Sesiones";
                apiServiveController.SendPost(url, Usuario, Nif);
                url = UrlAdress + "/Fichajes/FichajesEmpleado/" + Usuario + "/1?get_param=value";
                Conectado = apiServiveController.GetPost(url);
                url =  UrlAdress + "/Logout";
                apiServiveController.SendPost(url);
            }
        }
        Thread thread = new Thread(new PostRunableClass( urlAdress,  usuario, nif));
        thread.start();
        thread.join();
    }

    public void SetFichar(String urlAdress, String usuario, String nif) throws InterruptedException {
        class PostRunableClass implements Runnable {
            private String UrlAdress;
            private String Usuario;
            private String Nif;
            private PostRunableClass(String urlAdress, String usuario, String nif) { UrlAdress = urlAdress; Usuario = usuario; Nif = nif;}
            public void run() {
                ApiServiveController apiServiveController = new ApiServiveController();
                String url = UrlAdress + "/Sesiones";
                apiServiveController.SendPost(url, Usuario, Nif);
                url = UrlAdress + "/Fichajes/FichajesEmpleado/" + Usuario + "/1?get_param=value";
                Conectado = apiServiveController.GetPost(url);
                if (!Conectado) {
                    url = UrlAdress + "/Fichajes/IniciarFichajeEmpleado/" + Usuario;
                } else {
                    url = UrlAdress + "/Fichajes/FinalizarFichajeEmpleado/" + Usuario;
                }
                apiServiveController.SendPost(url);
                Conectado=!Conectado;
                url =  UrlAdress + "/Logout";
                apiServiveController.SendPost(url);
            }
        }
        Thread thread = new Thread(new PostRunableClass( urlAdress,  usuario, nif));
        thread.start();
        thread.join();
    }
}
