package com.foxdevs.foxtouch.Controllers;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

class ApiServiveController {
    private List<String> cookies;
    ApiServiveController() {
        CookieHandler.setDefault(new CookieManager());
    }
    private void setCookies(List<String> cookies) {
        if (cookies != null && this.cookies==null) {
            this.cookies = cookies;
        }
    }

    void SendPost(String urlAdress, String usuario, String nif)
    {
        try {
            URL url = new URL(urlAdress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setRequestProperty("Accept","application/json");
            if (cookies!=null  )
            {for (String cookie : this.cookies) {
                conn.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
            }}
            conn.setDoOutput(true);
            conn.setDoInput(true);

            JSONObject jsonParam = new JSONObject();
            jsonParam.put("Identificador", usuario);
            jsonParam.put("Nif",nif);

            Log.i("JSON", jsonParam.toString());
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            os.writeBytes(jsonParam.toString());

            os.flush();
            os.close();

            Log.i("STATUS", String.valueOf(conn.getResponseCode()));
            Log.i("MSG" , conn.getResponseMessage());
            setCookies(conn.getHeaderFields().get("Set-Cookie"));
            conn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void SendPost(String urlAdress)
    {
        try {
            URL url = new URL(urlAdress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setRequestProperty("Accept","application/json");
            if (cookies!=null  )
            {for (String cookie : this.cookies) {
                conn.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
            }}

            conn.setDoOutput(true);
            conn.setDoInput(true);

            JSONObject jsonParam = new JSONObject();
            Log.i("JSON", jsonParam.toString());
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            os.writeBytes(jsonParam.toString());

            os.flush();
            os.close();

            Log.i("STATUS", String.valueOf(conn.getResponseCode()));
            Log.i("MSG" , conn.getResponseMessage());
            setCookies(conn.getHeaderFields().get("Set-Cookie"));
            conn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    boolean GetPost(String urlAdress)
    {
        try {
            URL url = new URL(urlAdress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
             if (cookies!=null  )
            {for (String cookie : this.cookies) {
                conn.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
            }}

            Log.i("STATUS", String.valueOf(conn.getResponseCode()));
            Log.i("MSG" , conn.getResponseMessage());
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line+"\n");
            }
            br.close();
            JSONObject obj = new JSONArray(sb.toString()).getJSONObject(0);
            String valorFichaje=obj.getString("FechaSalida");
            conn.disconnect();
            return (valorFichaje.equals("null"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
