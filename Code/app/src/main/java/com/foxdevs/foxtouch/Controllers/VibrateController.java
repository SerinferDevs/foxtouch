package com.foxdevs.foxtouch.Controllers;

import android.content.Context;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.widget.Toast;

import static android.content.Context.VIBRATOR_SERVICE;
import static android.widget.Toast.LENGTH_LONG;

class VibrateController {
    private static final int Time =3000;

    static void Vibrate(Context context) {
        Vibrator vibrator = (Vibrator) context.getSystemService(VIBRATOR_SERVICE);
        if (vibrator != null && vibrator.hasVibrator()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                VibrationEffect effect = VibrationEffect.createOneShot(Time, VibrationEffect.DEFAULT_AMPLITUDE);
                vibrator.vibrate(effect);
            } else {
                vibrator.vibrate(Time);
            }
        } else {
            Toast.makeText(context, "Device does not support vibration", LENGTH_LONG).show();
        }
    }
}
