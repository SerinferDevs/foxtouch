package com.foxdevs.foxtouch.Controllers;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.foxdevs.foxtouch.Entities.PreferenceEntity;

import java.util.Date;

import static android.content.Context.MODE_PRIVATE;
import static android.widget.Toast.LENGTH_LONG;

public class PreferencesController {
    private static final String MY_PREFS_NAME = "FoxTouchPreferences";

    public static void SavePreferents( Context context,PreferenceEntity preferences, boolean mostrarAviso) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("usuario",preferences.Usuario);
        editor.putString("nif",  preferences.Nif);
        editor.putLong ("FechaUltimoFichaje",  System.currentTimeMillis());
        editor.apply();
        if (mostrarAviso)
        {Toast.makeText(context,"Guardado", LENGTH_LONG).show();}
    }

    public static PreferenceEntity LoadPreferents(Context context) {
        PreferenceEntity preference = new PreferenceEntity();
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        if (prefs!=null) {
            preference.Usuario = prefs.getString("usuario", "");
            preference.Nif = prefs.getString("nif", "");
            try {
                preference.FechaUltimoFichaje = new Date(prefs.getLong("FechaUltimoFichaje", 0));
            }catch (Exception e) {
                preference.FechaUltimoFichaje=new Date(0);
            }
        }else {
            preference.Usuario ="";
            preference.Nif = "";
            preference.FechaUltimoFichaje=new Date(0);
        }
        return  preference;
    }
}

